use std::fs;

pub fn solve() {
    let input_path =
        String::from("/home/maliur/projects/advent_of_code_2019/src/advent/day_1/input.txt");
    let input = fs::read_to_string(&input_path).expect("could not read from file");

    println!("----------Day 1------------");
    println!("Answer for part one is {:?}", part_one(&input));
    println!("Answer for part two is {:?}", part_two(&input));
}

fn part_one(input: &str) -> i64 {
    input
        .lines()
        .flat_map(|mass| mass.trim().parse::<f64>())
        .map(|mass| (((mass / 3.0).floor()) as i64) - 2)
        .sum()
}

fn part_two(input: &str) -> i64 {
    input
        .lines()
        .flat_map(|mass| mass.trim().parse::<f64>())
        .map(|mass| {
            let mut left_over = ((mass / 3.0).floor()) - 2.0;
            let mut total_fuel = 0;

            while left_over > 0.0 {
                total_fuel += left_over as i64;
                left_over = ((left_over / 3.0).floor()) - 2.0;
            }

            total_fuel
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        // Fuel required to launch a given module is based on its mass. Specifically, to find the
        // fuel required for a module, take its mass, divide by three, round down, and subtract 2.
        let input = "12\n14\n1969\n100756";
        let expected = 34241;

        assert_eq!(part_one(input), expected);
    }

    #[test]
    fn test_part_two() {
        // For each module mass, calculate its fuel and add it to the total. Then, treat the fuel
        // amount you just calculated as the input mass and repeat the process, continuing until a
        // fuel requirement is zero or negative.
        let input = "12\n14\n1969\n100756";
        let expected = 2 + 2 + 966 + 50346;

        assert_eq!(part_two(input), expected);
    }
}
