use std::fs;

pub fn solve() {
    let input_path =
        String::from("/home/maliur/projects/advent_of_code_2019/src/advent/day_2/input.txt");
    let input = fs::read_to_string(&input_path).expect("could not read from file");

    println!("----------Day 2------------");
    println!("Answer for part one is {:?}", part_one(&input));
    // println!("Answer for part two is {:?}", part_two(&input));
}

struct Computer {}

fn part_one(input: &str) -> usize {
    const INSTRUCTION_SIZE: usize = 4;

    let mut data: Vec<usize> = input
        .split(",")
        .flat_map(|x| x.trim().parse::<usize>())
        .collect();

    let mut pointer = 0;
    loop {
        let opcode = data[pointer];
        let parameter_one = data[data[pointer + 1]];
        let parameter_two = data[data[pointer + 2]];
        let paramater_three = data[pointer + 3];

        let sum = match opcode {
            1 => parameter_one + parameter_two,
            2 => parameter_one * parameter_two,
            _ => break,
        };

        data[paramater_three] = sum;
        pointer += INSTRUCTION_SIZE;
    }

    data[0]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        // one input is 4 bits
        // [
        //  Opcode, 1 = add 2 = multiply 99 = exit
        //  position of the value to add,
        //  position of the value to add,
        //  position where to place the sum
        // ]
        let input = "1,0,0,0,99,0,0,0";
        let expected = 2;

        assert_eq!(part_one(input), expected);
    }
}
