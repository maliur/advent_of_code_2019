mod advent;

fn main() {
    advent::day_1::solve();
    advent::day_2::solve();
}
